import java.sql.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;
import Requetes.Condition;
import Requetes.Create;
import Requetes.Insert;
import Requetes.Join;
import Requetes.Select;
import Utile.Utile;
import classes.Attribut;
import classes.Domaine;
import classes.Element;
import classes.Relation;
import file.Writer;
public class Main {
    public static void main(String[] args) throws Exception {
      
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                System.out.print("SQL>");
                String input = scanner.nextLine();
                if (input.equalsIgnoreCase("exit")) {
                    break;
                }
                else{
                    Object o =  Utile.doIt(input);
                    if (o instanceof String) {  
                        System.out.println(o);
                    }
                    if(o instanceof Relation) {
                        ((Relation) o).display();
                    }
                }
            } 
        }  
         /* Create c = new Create();
        c.create("creer la table eleve : nom // text , prenom //text . "); 
         Insert i = new Insert();
        i.insert("inserer un element dans eleve : nom // Fitiavana , prenom // Mahaliana. "); */ 
        /* Select s = new Select(); 
        Relation eleves = s.select("tous les elements dans  test : nom .");
        eleves.display(); 
        Relation prof = s.select("tous les elements dans eleve : nom .");
        prof.display();
        Join j = new Join (); 
        Relation r = eleves.union(prof, "null") ;//join("tous les elements dans la jointure de eleve et prof : nom = nom . ");
        r.display(); */
     //  Condition c = new Condition("age = ma",eleves); 
            //Relation profs = s.select("tous les elements dans la table prof : toutes les colonnes. ");
            ////profs.display();
     //  eleves.display();
            //Relation inter = eleves.intersection(profs,"union");
            //inter.display();
            //Relation diff = eleves.difference(profs,"union");
            //diff.display();
            /*Domaine d = new Domaine();
            d.setClasses("java.lang.String");
            Domaine d1 = new Domaine();
            d1.setClasses("java.lang.Integer");        
            Attribut a = new Attribut();
            a.setNom("nom");
            a.setDomaine(d);
            Attribut b = new Attribut();
            b.setNom("age");
            b.setDomaine(d1);
            Attribut c = new Attribut();
            c.setNom("prenom");
            c.setDomaine(d);
            Relation r = 
            HashMap h = new HashMap<>();
            h.put(a, "FFFahaliana");
            h.put(c, "Fitiavana");
            h.put(b, "10");
            Element e = new Element();
            e.setValue(h);
            System.out.println(e.isIn(t, t));*/


    }
}