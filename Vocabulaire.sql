domaine : text , entier , date 
-----------------------creation-de-table------------------------------
creer la table table_name : nom_attribut1 // type , nom_attribut2 // type   .
ex: creer la table eleve : nom // text , age // entier.
    creer la table prof : nom // text , age // entier.


-----------------------insertion-d'un-element-------------------------
inserer un element dans table_name : nom_attribut1 // valeur1 , nom_attribut2 // valeur2 .
ex: inserer un element dans  eleve : nom // Mahaliana , age  // 20 .
    inserer un element dans  prof : nom // KOTO , age  // 20 .

-------------------------------select---------------------------------
tous les elements dans  table_name : toutes les colonnes .
ex: tous les elements dans eleve : toutes les colonnes.


------------------------------select--colonnes------------------------
tous les elements dans  table_name : colonne1, colonne2 , ... .
ex: tous les elements dans  eleve : age , nom .

------------------------------select--WHERE---------------------------
tous les elements dans  table_name : toutes les colonnes :  colonne (<,<=,>,>=,=,<>) value.
ex: tous les elements dans  test : toutes les colonnes : age >= 20 .

------------------------------join------------------------------------
jointure de table_name1 et table_name2 : table_name1_colonne (<,<=,>,>=,=,<>)  table_name2_colonne : toutes les colones .
ex: jointure de eleve et prof : nom = nom  : toutes les colones.

------------------------------join naturel------------------------------------
jointure de table_name1 et table_name2 : toutes les colones.
ex: jointure de eleve et prof : toutes les colones .

------------------------------union------------------------------------
union de table_name1 et table_name2 : table_name1.colonne1, table_name1.colonne2, colonne3 .
ex : union de eleve et prof : toutes les colones .



